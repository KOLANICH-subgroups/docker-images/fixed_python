# syntax=docker/dockerfile:experimental
FROM registry.gitlab.com/kolanich-subgroups/docker-images/cpp:latest
LABEL maintainer="KOLANICH"
WORKDIR /tmp
ADD ./*.sh ./
ADD ./.ci/pythonPackagesToInstallFromIpi.txt ./
ADD ./installPythonPackagesFromIpi.sh ./
ADD ./fix_compiler.py ./
#ADD ./fix_python_modules_paths.py ./
ADD ./pip.conf ./
# todo: pypy
RUN \
	set -ex;\
	mv ./pip.conf /etc/;\
	./pythonStdlibFixes.sh ;\
	apt-get update ;\
	apt-get install -y python3 python3-distutils libpython3-dev lsb-release python-is-python3;\
	python3 fix_compiler.py;\
	\
	apt-get install -y libffi-dev python3-apt python3-gpg python3-lxml python3-bs4 python3-certifi python3-cryptography mercurial;\
	\
	git clone --depth=50 https://github.com/KOLANICH-tools/ipi.py;\
	export SUDO=;\
	cd ./ipi.py;\
	python3 -m ipi bootstrap packaging;\
	python3 -m ipi bootstrap itself;\
	cd ..;\
	rm -rf ./ipi.py;\
	\
	. ./installPythonPackagesFromIpi.sh ;\
	#python3 ./fix_python_modules_paths.py;\
	ipi repo add -y https://github.com/KOLANICH/ipi-repo;\
	\
	apt-get autoremove --purge -y ;\
	apt-get clean && rm -rf /var/lib/apt/lists/* ;\
	rm -rf ~/.cache/pip ;\
	rm -rf /tmp/*
